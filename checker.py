"""
..module: .checker.py
..description: Classes to handle data validation.

..author: Arthur Moore <arthur.moore85@gmail.com>
..date: 2021-12-07
"""
from datetime import datetime
import re
import yaml


class Validator:
    def __init__(self, *args, **kwargs):
        self.source_file = kwargs.get("source", "checker_settings.yml")
        self.source_data = self._load_settings()
        self._data_validators = {
            'bool': self._bool_validator,
            'string': self._string_validator,
            'integer': self._validator_int,
            'email': self._validator_email,
            'float': self._validator_float,
            'yn': self._validator_yes_no,
            'dictionary': self._validator_dict,
            'list': self._validator_list,
            'date': self._validator_date,
            'time': self._validate_time,
        }

    def _load_settings(self):
        """
        Loads the settings from the provided yaml file.
        """
        with open(self.source_file, "r") as data_stream:
            try:
                fields = yaml.safe_load(data_stream).get('fields')
                if not fields:
                    fields = {}
                return fields
            except yaml.YAMLError as exc:
                raise IOError("Settings YAML file is invalid")

    def validate(self, fieldname, value):
        """
        Handles validation.
        """
        if not self.source_data.get(fieldname):
            raise ValueError("This field is not found in the settings yaml.")

        data_entry = self.source_data.get(fieldname)
        data_type = data_entry.get('datatype')
        if data_type:
            valid = self._data_validators[data_type](fieldname, value)
            required = data_entry.get('required')
            if valid:
                if required and not value:
                    valid = False
            if not required:
                valid = True
            
            return valid
        else:
            raise ValueError(f"No datatype found for {fieldname}")

    def _validator_dict(self, field, value):
        """
        Verifies that the value is a dictionary
        """
        return isinstance(value, dict)

    def _validator_list(self, field, value):
        """
        Verifies that the value is a list.
        """
        return isinstance(value, list)

    def _validator_date(self, field, value):
        """
        Verifies that the value is of a correct date.
        """
        max_length = self.source_data.get(field).get("format")
        try:
            datetime.strptime(value, max_length)
            return True
        except:
            return False

    def _validator_time(self, field, value):
        """
        Verifies that the value is the correct time format.
        """
        try:
            datetime.strptime(value, "%H:%M")
            return True
        except:
            return False

    def _bool_validator(self, field, value):
        """
        Verifies that the value is a boolean
        """
        return isinstance(value, bool)

    def _string_validator(self, field, value):
        """
        Verifies that the value is a string and has a passes max length, if
        provided.
        """
        max_length = self.source_data.get(field).get("length")
        validated = False
        if isinstance(value, str):
            if max_length:
                if len(value) <= max_length:
                    validated = True
            else:
                validated = True

        return validated

    def _validator_int(self, field, value):
        """
        Verifies that the value is an integer and is between a minimum and maximum
        integer if provided.
        """
        field_data = self.source_data.get(field)
        minimum = field_data.get("minimum")
        maximum = field_data.get("maximum")
        validated = True
        if not isinstance(value, int):
            validated = False
        else:
            if minimum and value < minimum:
                validated = False
            if maximum and value > maximum:
                validated = False
        
        return validated

    def _validator_email(self, field, value):
        """
        Verifies that the value is a valid email.
        """
        if not isinstance(value, str):
            return False

        return re.match(r"[^@]+@[^@]+\.[^@]+", value)

    def _validator_float(field, value):
        """
        Verifies that the value is a valid float.
        """
        return isinstance(value, float)

    def _validator_yes_no(self, field, value):
        """
        Verifies that the field contains yes or no.
        It also checks if the expected value is reached if
        provided.
        """
        validated = True
        source_data = self.source_data.get(field)
        if "only" in source_data.keys():
            only = str(self.source_data.get(field).get("only")).lower()
        else:
            only = None
        yes = ("yes", "y")
        no = ("no", "n")
        is_yes = only in yes
        is_no = only in no
        if not isinstance(value, str):
            return False
        
        if not value.lower() in yes and not value.lower() in no:
            return False
        
        if only and not only.lower() in yes and not only in no:
            raise ValueError("'only' keyword is not yes or no")
        elif only:
            if is_yes:
                if not value.lower() in yes:
                    validated = False
            if is_no:
                if not value.lower() in no:
                    validated = False
        
        return validated
