Datacheck
=========
A data integrity validator for data in Python.

It uses a yaml file that lists the data setup to check data integrity against.

Usage
-------------

To use this app, you must set up a `checker_settings.yml` file in your project directory.

The yaml file start with the key `fields`.

Within the `fields` section you will specify the fields you will use. Each field will have various keys that specify what conditions must be met for data to pass validation.

The following options are manditory for every field:

* `required` - Whether the data for this field is required or not (true/false)
* `datatype` - What datatype this field must be, a failure will result in a raised exception. See below for supported datatypes.

There are some optional extra options that apply to specific datatypes which will be provided below.

The following datatypes are supported:

* `bool` - Boolean
* `string` - String
* `integer` - Integer
* `email` - Not technically a datatype, but validates an email address
* `yn` - Yes or No (y/n)
* `dictionary` - Dictionary (dict)
* `list` - List
* `date` - A date string
* `time` - A time string

Here they are in detail:

Bool
-----------
A boolean filetype has one manditory field:
`required` (true/false).

This has no extra options

String
-----------
A String filetype has one manditory field: `required` (true/false).

This has one optional field: `length` - maximum length of string allowed.

Integer
--------
An Integer filetype has one manditory field: `required` (true/false).

This has two optional field: `minimum` - the minimum value the integer must be. `maximum` the maximum value accepted.

Email
-----
An Email filetype has one manditory field: `required` (true/false).

This has no optional fields

YN
--
A YN filetype has one manditory field: `required` (true/false).

This has one optional field: `only` - what value is required to pass validation (y/n).

Dictionary
----------
A Dictionary has one mandatory field: `required` (true/false).

This has no optional fields.

List
----
A List filetype has one mandatory field: `required` (true/false).

This has no optional fields.

Date
----
A Date filetype has one manditory field: `required` (true/false).

This has one optional field: `format` - What format you expect the date in.
The following can be represented:
* %Y - Full year format eg. 2001
* %m - Two digit month format eg. 12
* %d - Two digit day format eg. 01

To represent 2001-12-01 you would enter %Y-%m-%d


Time
----
A Time filetype has one mandatory field: `required` (true/false).

This has no optional fields.

Example
-------
The following is an example yaml file (provided in the repository):

```yaml
fields:
  name:
    required: true
    datatype: string
    length: 10
  age:
    required: true
    datatype: integer
    minimum: 13
  email:
    required: true
    datatype: email
  is_happy:
    required: yes
    datatype: yn
    only: y
  active:
    required: false
    datatype: bool
  start_date:
    required: true
    datatype: date
    format: "%d-%m-%Y"
  example_list:
    required: true
    datatype: list
  example_dict:
    required: true
    datatype: dictionary
  example_time:
    required: true
    datatype: time
```

In your Python file you would run:

```python
from checker import Validator

validator = Validator(source='location/to/checker_settings.yml')
validator.validate('age', 10)
# This will fail as minimum is 13. Returns False
validator.validate('age', 15)
# This will pass. Returns True
validator.validate('name', 'Example')
# This will pass. Returns True
```